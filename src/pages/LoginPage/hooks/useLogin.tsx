import { useSnackbar } from 'notistack';
import React from 'react';
import { useForm } from 'react-hook-form';
import { useHistory } from 'react-router-dom';
import { RoutesString } from 'routes/RoutesString';
import useAuthStore from 'stores/auth.store';

interface ILoginFormValues {
  username: string;
  password: string;
}

function useLogin() {
  const { enqueueSnackbar } = useSnackbar();
  const { push } = useHistory();
  const { loginAction } = useAuthStore();
  const { register, handleSubmit: _handleSubmit } = useForm<ILoginFormValues>({
    defaultValues: {
      username: '',
      password: '',
    },
  });

  const handleSubmit = _handleSubmit(({ username, password }) => {
    if (username === 'admin' || password === 'admin') {
      enqueueSnackbar({
        variant: 'success',
        message: 'Login successfully!',
      });
      loginAction();
      push(RoutesString.Home);
      return;
    }

    enqueueSnackbar({
      variant: 'error',
      message: 'Incorrect',
    });
  });

  return { handleSubmit, register };
}

export default useLogin;

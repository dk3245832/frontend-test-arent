import React from 'react';

// * Styles
import styled from 'styled-components';

// * components
import { Library, Recommend } from './components';

const ColumnPage = () => {
  return (
    <HomePageWrapper>
      <div className="container">
        <Recommend />
        <Library />
      </div>
    </HomePageWrapper>
  );
};

const HomePageWrapper = styled.div`
  .container {
    width: 100%;
    max-width: 990px;
    padding-right: 15px;
    padding-left: 15px;
    margin-right: auto;
    margin-left: auto;
    position: relative;
  }
`;

export default ColumnPage;

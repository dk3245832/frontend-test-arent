import React from "react";

// * Styles
import styled from "styled-components";

const Recommend = () => {
  const dataRecommend = [
    {
      title: (
        <p className="title">
          RECOMMENDED<span>COLUMN</span>
        </p>
      ),
      regime: "オススメ",
    },
    {
      title: (
        <p className="title">
          RECOMMENDED<span>DIET</span>
        </p>
      ),
      regime: "ダイエット",
    },
    {
      title: (
        <p className="title">
          RECOMMENDED<span>BEAUTY</span>
        </p>
      ),
      regime: "美容",
    },
    {
      title: (
        <p className="title">
          RECOMMENDED<span>HEALTH</span>
        </p>
      ),
      regime: "健康",
    },
  ];

  return (
    <RecommendWrapper>
      <div className="items">
        {dataRecommend.map((item, index) => (
          <div className="item" key={index}>
            <span className="item_title">{item.title}</span>
            <p className="line"></p>
            <span>{item.regime}</span>
          </div>
        ))}
      </div>
    </RecommendWrapper>
  );
};

const RecommendWrapper = styled.div`
  margin-top: 56px;

  .items {
    margin-bottom: 56px;
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: row;
    flex-wrap: wrap;
    gap: 32px;
    margin-bottom: 28px;
  }

  .item {
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    width: 200px;
    padding: 24px 8px 22px 8px;
    background-color: #2e2e2e;

    img {
      max-width: 40px;
    }

    .line {
      width: 56px;
      height: 1px;
      background-color: #ffffff;
      margin: 10px 0;
    }

    p {
      margin-bottom: 0;

      &.title {
        color: #ffcc21;
        text-align: center;
        font-family: Inter;
        font-size: 22px;
        font-style: normal;
        font-weight: 400;
        line-height: 27px;
        letter-spacing: 0.11px;

        span {
          color: #ffcc21;
          display: block;
          font-weight: 400;
          font-size: 22px;
          letter-spacing: 0.11px;
        }
      }
    }

    span {
      color: #ffffff;
      text-align: center;
      font-family: "HiraginoKakuGothicPro";
      font-size: 18px;
      font-style: normal;
      font-weight: 300;
      line-height: 26px;
    }
  }
`;

export default Recommend;

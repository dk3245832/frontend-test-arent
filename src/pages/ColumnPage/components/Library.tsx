import React from "react";

// * Styles
import styled from "styled-components";

// * Images Thumb
import ThumbOne from "assets/images/column-1.jpg";
import ThumbTwo from "assets/images/column-2.jpg";
import ThumbThree from "assets/images/column-3.jpg";
import Four from "assets/images/column-4.jpg";
import ThumbFive from "assets/images/column-5.jpg";
import ThumbSix from "assets/images/column-6.jpg";
import ThumbSeven from "assets/images/column-7.jpg";
import ThumbEight from "assets/images/column-8.jpg";

const Library = () => {
  const data = [
    {
      img: `${ThumbOne}`,
      date: "05.21.Morning",
      desc: "魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメリ…",
      tag: "#魚料理  #和食  #DHA",
    },
    {
      img: `${ThumbTwo}`,
      date: "05.21.Lunch",
      desc: "魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメリ…",
      tag: "#魚料理  #和食  #DHA",
    },
    {
      img: `${ThumbThree}`,
      date: "05.21.Dinner",
      desc: "魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメリ…",
      tag: "#魚料理  #和食  #DHA",
    },
    {
      img: `${Four}`,
      date: "05.21.Snack",
      desc: "魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメリ…",
      tag: "#魚料理  #和食  #DHA",
    },
    {
      img: `${ThumbFive}`,
      date: "05.21.Morning",
      desc: "魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメリ…",
      tag: "#魚料理  #和食  #DHA",
    },
    {
      img: `${ThumbSix}`,
      date: "05.20.Lunch",
      desc: "魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメリ…",
      tag: "#魚料理  #和食  #DHA",
    },
    {
      img: `${ThumbSeven}`,
      date: "05.20.Dinner",
      desc: "魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメリ…",
      tag: "#魚料理  #和食  #DHA",
    },
    {
      img: `${ThumbEight}`,
      date: "05.21.Snack",
      desc: "魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメリ…",
      tag: "#魚料理  #和食  #DHA",
    },
  ];

  return (
    <FoodLibraryWrapper>
      <div className="items">
        {data.map((item, index) => (
          <div className="item">
            <div className="item_pic" key="index">
              <img src={item.img} alt="" />
              <span>{item.date}</span>
            </div>
            <div className="item_content">
              <p className="item_desc">{item.desc}</p>
              <p className="item_tag">{item.tag}</p>
            </div>
          </div>
        ))}
      </div>
      <button className="btn_more">コラムをもっと見る</button>
    </FoodLibraryWrapper>
  );
};

const FoodLibraryWrapper = styled.div`
  margin-bottom: 64px;

  .items {
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: row;
    flex-wrap: wrap;
    gap: 8px;
    margin-bottom: 28px;
  }

  .item {
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    width: 234px;
    height: 234px;
    overflow: hidden;

    &_pic {
      position: relative;
      margin-bottom: 8px;

      img {
        width: 100%;
        height: 100%;
      }

      span {
        position: absolute;
        bottom: 0;
        left: 0;
        background: #ffcc21;
        padding: 7px 0 7px 8px;
        width: 120px;
        color: #fff;
        font-family: Inter;
        font-size: 15px;
        font-style: normal;
        font-weight: 400;
        line-height: 18px;
        letter-spacing: 0.15px;
      }
    }

    &_content {
      color: #414141;
      font-family: Hiragino Kaku Gothic Pro;
      font-size: 15px;
      font-style: normal;
      font-weight: 300;
      line-height: 22px;
      letter-spacing: 0.075px;
    }

    &_desc {
      margin-bottom: 0;
    }

    &_tag {
      color: #ff963c;
      font-family: Hiragino Kaku Gothic Pro;
      font-size: 12px;
      font-style: normal;
      font-weight: 300;
      line-height: 22px;
      letter-spacing: 0.06px;
    }
  }

  .btn_more {
    background: linear-gradient(33deg, #ffcc21 8.75%, #ff963c 86.64%);
    color: #fff;
    text-align: center;
    font-family: Hiragino Kaku Gothic Pro;
    font-size: 18px;
    font-style: normal;
    font-weight: 300;
    line-height: 26px;
    width: 296px;
    padding: 15px 0;
    display: flex;
    align-items: center;
    justify-content: center;
    margin: 0 auto;
    border: none;
    cursor: pointer;
    transition: 0.5s;

    &:hover {
      transition: 0.5s;
      opacity: 0.5;
      border: none;
      color: #fff;
      transition: 0.5s;
    }
  }
`;

export default Library;

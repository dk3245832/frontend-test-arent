export { default as ColumnPage } from './ColumnPage';
export { default as HomePage } from './HomePage';
export { default as MyRecordPage } from './MyRecordPage';
export { default as LoginPage } from './LoginPage';

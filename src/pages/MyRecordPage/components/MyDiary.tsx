import React from "react";

// * Styles
import styled from "styled-components";

const MyDiary = () => {
  const data = [
    {
      date: "2021.05.21",
      hours: "23:25",
      title: "私の日記の記録が一部表示されます。",
      content:
        "テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト…",
    },
    {
      date: "2021.05.21",
      hours: "23:25",
      title: "私の日記の記録が一部表示されます。",
      content:
        "テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト…",
    },
    {
      date: "2021.05.21",
      hours: "23:25",
      title: "私の日記の記録が一部表示されます。",
      content:
        "テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト…",
    },
    {
      date: "2021.05.21",
      hours: "23:25",
      title: "私の日記の記録が一部表示されます。",
      content:
        "テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト…",
    },
    {
      date: "2021.05.21",
      hours: "23:25",
      title: "私の日記の記録が一部表示されます。",
      content:
        "テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト…",
    },
    {
      date: "2021.05.21",
      hours: "23:25",
      title: "私の日記の記録が一部表示されます。",
      content:
        "テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト…",
    },
    {
      date: "2021.05.21",
      hours: "23:25",
      title: "私の日記の記録が一部表示されます。",
      content:
        "テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト…",
    },
    {
      date: "2021.05.21",
      hours: "23:25",
      title: "私の日記の記録が一部表示されます。",
      content:
        "テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト…",
    },
  ];

  return (
    <MyDiaryWrapper>
      <h2>MY DIARY</h2>
      <div className="items">
        {data.map((item, index) => (
          <div className="item" key="index">
            <span>{item.date}</span>
            <span>{item.hours}</span>
            <div className="item_content">
              <p>{item.title}</p>
              <p>{item.content}</p>
            </div>
          </div>
        ))}
      </div>
      <button className="btn_more">自分の日記をもっと見る</button>
    </MyDiaryWrapper>
  );
};

const MyDiaryWrapper = styled.div`
  margin-bottom: 64px;

  h2 {
    color: #414141;
    font-family: Inter;
    font-size: 22px;
    font-style: normal;
    font-weight: 400;
    line-height: 27px;
    letter-spacing: 0.11px;
  }

  .items {
    display: flex;
    justify-content: space-between;
    align-items: center;
    flex-direction: row;
    flex-wrap: wrap;
    row-gap: 12px;
    margin-bottom: 28px;
  }

  .item {
    display: flex;
    flex-direction: column;
    width: 200px;
    position: relative;
    overflow: hidden;
    padding: 16px 16px 27px 16px;
    border: 2px solid #707070;

    span {
      color: #414141;
      font-family: Inter;
      font-size: 18px;
      font-style: normal;
      font-weight: 400;
      line-height: 22px;
      letter-spacing: 0.09px;
      text-align: left;
    }

    &_content {
      color: #414141;
      font-family: Hiragino Kaku Gothic Pro;
      font-size: 12px;
      font-style: normal;
      font-weight: 300;
      line-height: 17px;
      letter-spacing: 0.06px;
      margin-top: 8px;
    }
  }

  .btn_more {
    background: linear-gradient(33deg, #ffcc21 8.75%, #ff963c 86.64%);
    color: #fff;
    text-align: center;
    font-family: Hiragino Kaku Gothic Pro;
    font-size: 18px;
    font-style: normal;
    font-weight: 300;
    line-height: 26px;
    width: 296px;
    padding: 15px 0;
    display: flex;
    align-items: center;
    justify-content: center;
    margin: 0 auto;
    border: none;
    border-radius: 4px;
    cursor: pointer;
    transition: 0.5s;

    &:hover {
      transition: 0.5s;
      opacity: 0.5;
      border: none;
      color: #fff;
      transition: 0.5s;
    }
  }
`;

export default MyDiary;

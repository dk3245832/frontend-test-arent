import React from "react";

// * Styles
import styled from "styled-components";

const MyExercise = () => {
  const dataExercise = [
    {
      title: "家事全般（立位・軽い",
      kcal: "26kcal",
      minutes: "10 min",
    },
    {
      title: "家事全般（立位・軽い",
      kcal: "26kcal",
      minutes: "10 min",
    },
    {
      title: "家事全般（立位・軽い",
      kcal: "26kcal",
      minutes: "10 min",
    },
    {
      title: "家事全般（立位・軽い",
      kcal: "26kcal",
      minutes: "10 min",
    },
    {
      title: "家事全般（立位・軽い",
      kcal: "26kcal",
      minutes: "10 min",
    },
    {
      title: "家事全般（立位・軽い",
      kcal: "26kcal",
      minutes: "10 min",
    },
    {
      title: "家事全般（立位・軽い",
      kcal: "26kcal",
      minutes: "10 min",
    },
    {
      title: "家事全般（立位・軽い",
      kcal: "26kcal",
      minutes: "10 min",
    },
  ];

  return (
    <MyExerciseWrapper>
      <div className="head-table">
        <h2>
          MY<span>EXERCISE</span>
        </h2>
        <p>2021.05.21</p>
      </div>
      <ul className="items">
        {dataExercise.map((item, index) => (
          <li className="item" key={index}>
            <div className="item_box">
              <p>{item.title}</p>
              <span>{item.kcal}</span>
            </div>
            <div className="item_minutes">{item.minutes}</div>
          </li>
        ))}
      </ul>
    </MyExerciseWrapper>
  );
};

const MyExerciseWrapper = styled.div`
  background: #414141;
  margin-bottom: 56px;
  padding: 16px 56px 24px 24px;

  .head-table {
    display: flex;
    column-gap: 23px;
    margin-bottom: 4px;

    h2 {
      color: #fff;
      font-family: Inter;
      font-size: 15px;
      font-style: normal;
      font-weight: 400;
      line-height: 18px;
      letter-spacing: 0.15px;

      span {
        display: block;
      }
    }

    p {
      color: #fff;
      font-family: Inter;
      font-size: 22px;
      font-style: normal;
      font-weight: 400;
      line-height: 27px;
      letter-spacing: 0.11px;
    }
  }
  .items {
    display: grid;
    grid-template-columns: 1fr 1fr;
    row-gap: 8px;
    column-gap: 40px;
    height: 180px;
    overflow-y: scroll;
    padding-right: 32px;

    &::-webkit-scrollbar-track {
      border-radius: 10px;
      background-color: #777777;
      -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
    }

    &::-webkit-scrollbar {
      width: 6px;
    }

    &::-webkit-scrollbar-thumb {
      border-radius: 10px;
      background-color: #ffcc21;
    }

    .item {
      display: flex;
      justify-content: space-between;
      align-items: center;
      border-bottom: 1px solid #777777;
      padding-left: 16px;
      padding-bottom: 2px;
      position: relative;

      &:before {
        content: "";
        width: 7px;
        height: 7px;
        border-radius: 50%;
        background-color: #ffffff;
        position: absolute;
        top: 5px;
        left: 0;
      }

      &_box {
        p {
          color: #fff;
          font-family: Hiragino Kaku Gothic Pro;
          font-size: 15px;
          font-style: normal;
          font-weight: 300;
          line-height: 22px;
          letter-spacing: 0.075px;
        }

        span {
          color: #ffcc21;
          font-family: Inter;
          font-size: 15px;
          font-style: normal;
          font-weight: 400;
          line-height: 18px;
          letter-spacing: 0.075px;
        }
      }

      &_minutes {
        color: #ffcc21;
        text-align: right;
        font-family: Inter;
        font-size: 18px;
        font-style: normal;
        font-weight: 400;
        line-height: 22px;
        letter-spacing: 0.09px;
      }
    }
  }
`;

export default MyExercise;

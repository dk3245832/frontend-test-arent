import React, { useEffect, useRef } from "react";
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend,
} from "chart.js";
import { Line } from "react-chartjs-2";

// * Styles
import styled from "styled-components";

const faker = require("faker");

ChartJS.register(
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend
);

export const options = {
  responsive: true,
  plugins: {
    legend: {
      position: "top" as const,
      display: false,
    },
    title: {
      display: true,
    },
  },
};

const labels = [
  "1月",
  "2月",
  "3月",
  "4月",
  "5月",
  "6月",
  "7月",
  "8月",
  "9月",
  "10月",
  "11月",
  "12月",
];

export const data = {
  labels,
  datasets: [
    {
      label: "Dataset 1",
      data: labels.map(() => faker.datatype.number({ min: -1000, max: 1000 })),
      borderColor: "#FFCC21",
      backgroundColor: "#FFCC21",
    },
    {
      label: "Dataset 2",
      data: labels.map(() => faker.datatype.number({ min: -1000, max: 1000 })),
      borderColor: "#8FE9D0",
      backgroundColor: "#8FE9D0",
    },
  ],
};

const Chart = () => {
  return (
    <MyExerciseWrapper>
      <div className="head-table">
        <h2>
          MY<span>EXERCISE</span>
        </h2>
        <p>2021.05.21</p>
      </div>
      <Line id="myChart" options={options} data={data} />
      <div className="date-box">
        <div>日</div>
        <div>週</div>
        <div>月</div>
        <div>年</div>
      </div>
    </MyExerciseWrapper>
  );
};

const MyExerciseWrapper = styled.div`
  background: #414141;
  margin-bottom: 56px;
  padding: 16px 56px 24px 24px;

  .date-box {
    display: flex;
    column-gap: 12px;
    margin-top: 9px;

    > div {
      border-radius: 14px;
      background: #fff;
      width: 56px;
      padding: 5px 0;
      text-align: center;
      color: #ffcc21;

      &:last-child {
        background: #ffcc21;
        color: #fff;
      }
    }
  }

  .head-table {
    display: flex;
    column-gap: 23px;
    margin-bottom: 4px;

    h2 {
      color: #fff;
      font-family: Inter;
      font-size: 15px;
      font-style: normal;
      font-weight: 400;
      line-height: 18px;
      letter-spacing: 0.15px;

      span {
        display: block;
      }
    }

    p {
      color: #fff;
      font-family: Inter;
      font-size: 22px;
      font-style: normal;
      font-weight: 400;
      line-height: 27px;
      letter-spacing: 0.11px;
    }
  }
`;

export default Chart;

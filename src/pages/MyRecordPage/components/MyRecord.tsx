import React from "react";

// * Styles
import styled from "styled-components";

// * Icon
import Mybody from "assets/images/MyRecommend-1.jpg";
import MyExercise from "assets/images/MyRecommend-2.jpg";
import MyDiary from "assets/images/MyRecommend-3.jpg";

const MyRecord = () => {
  const dataMyrecord = [
    {
      img: `${Mybody}`,
      title: "BODY RECORD",
      btnText: "自分のカラダの記録",
    },
    {
      img: `${MyExercise}`,
      title: "MY EXERCISE",
      btnText: "自分の運動の記録",
    },
    {
      img: `${MyDiary}`,
      title: "MY DIARY",
      btnText: "自分の日記",
    },
  ];
  return (
    <MyRecordWrapper>
      <MyRecordItem>
        {dataMyrecord.map((item, index) => (
          <div className="item" key={index}>
            <div className="item_pic">
              <img src={item.img} alt="" />
            </div>
            <div className="item_content">
              <span>{item.title}</span>
              <button>{item.btnText}</button>
            </div>
          </div>
        ))}
      </MyRecordItem>
    </MyRecordWrapper>
  );
};

const MyRecordWrapper = styled.div`
  margin: 56px 0;
`;

const MyRecordItem = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-bottom: 24px;
  column-gap: 48px;

  .item {
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    width: 240px;
    height: 240px;
    background-color: #ffcc21;
    padding: 24px;
    position: relative;

    span {
      color: #ffcc21;
      text-align: center;
      font-family: Inter;
      font-size: 25px;
      font-style: normal;
      font-weight: 400;
      line-height: 30px;
      letter-spacing: 0.125px;
      white-space: nowrap;
      margin-bottom: 11px;
    }

    &_pic {
      position: relative;
      width: 100%;
      height: 100%;

      &:before {
        content: "";
        background: #313131b0;
        width: 100%;
        height: 100%;
        display: block;
        position: absolute;
        top: 0;
        left: 0;
        z-index: 2;
      }

      img {
        width: 100%;
        height: 100%;
        filter: grayscale(100%);
      }
    }

    &_content {
      position: absolute;
      top: 50%;
      left: 50%;
      z-index: 3;
      transform: translate(-50%, -50%);
      text-align: center;
      span {
        margin-bottom: 11px;
        display: block;
      }
      & button {
        background-color: #ff963c;
        color: #fff;
        text-align: center;
        font-family: Hiragino Kaku Gothic Pro;
        font-size: 14px;
        font-style: normal;
        font-weight: 300;
        line-height: 20px;
        width: 160px;
        padding: 3px 0;
        border: none;
      }
    }
  }
`;

export default MyRecord;

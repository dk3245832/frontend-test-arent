import React from "react";

// * Styles
import styled from "styled-components";

// * components
import { FoodAndDrink, Banner, FoodLibrary } from "./components";

const HomePage = () => {
  return (
    <HomePageWrapper>
      <Banner />
      <div className="container">
        <FoodAndDrink />
        <FoodLibrary />
      </div>
    </HomePageWrapper>
  );
};

const HomePageWrapper = styled.div`
  .container {
    width: 100%;
    max-width: 990px;
    padding-right: 15px;
    padding-left: 15px;
    margin-right: auto;
    margin-left: auto;
    position: relative;
  }
`;

export default HomePage;

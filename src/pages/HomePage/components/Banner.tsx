import React from 'react';

// * Styles
import styled from 'styled-components';

// * Icon
import ImgBanner from 'assets/images/d01.jpg';

// * Components
import Chart from '../../../components/molecules/Chart';
import CirclePercent from '../../../components/molecules/CirclePercent/CirclePercent';

const Banner = () => {
  return (
    <BannerWrapper>
      <BannerContent>
        <div className="thumb_box">
          <img src={ImgBanner} alt="banner" />
          <CirclePercent clockwise={true} />
        </div>
        <Chart />
      </BannerContent>
    </BannerWrapper>
  );
};

const BannerWrapper = styled.div`
  margin-bottom: 24px;
  margin: 0 auto 24px auto;
`;

const BannerContent = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  height: 420px;
  overflow: hidden;

  .thumb_box {
    position: relative;

    & > div {
      position: absolute !important;
      top: 50%;
      left: 50%;
      z-index: 2;
      transform: translate(-50%, -50%);
    }
  }
`;

export default Banner;

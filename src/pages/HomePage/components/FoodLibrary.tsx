import React from "react";

// * Styles
import styled from "styled-components";

// * Images Thumb
import ThumbOne from "assets/images/m01.jpg";
import ThumbTwo from "assets/images/l03.jpg";
import ThumbThree from "assets/images/d01.jpg";
import Four from "assets/images/l01.jpg";
import ThumbFive from "assets/images/m01.jpg";
import ThumbSix from "assets/images/l02.jpg";
import ThumbSeven from "assets/images/d02.jpg";
import ThumbEight from "assets/images/s01.jpg";

const FoodLibrary = () => {
  const data = [
    {
      img: `${ThumbOne}`,
      date: "05.21.Morning",
    },
    {
      img: `${ThumbTwo}`,
      date: "05.21.Lunch",
    },
    {
      img: `${ThumbThree}`,
      date: "05.21.Dinner",
    },
    {
      img: `${Four}`,
      date: "05.21.Snack",
    },
    {
      img: `${ThumbFive}`,
      date: "05.21.Morning",
    },
    {
      img: `${ThumbSix}`,
      date: "05.20.Lunch",
    },
    {
      img: `${ThumbSeven}`,
      date: "05.20.Dinner",
    },
    {
      img: `${ThumbEight}`,
      date: "05.21.Snack",
    },
  ];

  return (
    <FoodLibraryWrapper>
      <div className="items">
        {data.map((item, index) => (
          <div className="item" key="index">
            <img src={item.img} alt="" />
            <span>{item.date}</span>
          </div>
        ))}
      </div>
      <button className="btn_more">記録をもっと見る</button>
    </FoodLibraryWrapper>
  );
};

const FoodLibraryWrapper = styled.div`
  margin-bottom: 64px;

  .items {
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: row;
    flex-wrap: wrap;
    gap: 8px;
    margin-bottom: 28px;
  }

  .item {
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    width: 234px;
    height: 234px;
    position: relative;
    overflow: hidden;

    img {
      width: 100%;
      height: 100%;
    }

    span {
      position: absolute;
      bottom: 0;
      left: 0;
      background: #ffcc21;
      padding: 7px 0 7px 8px;
      width: 120px;
      color: #fff;
      font-family: Inter;
      font-size: 15px;
      font-style: normal;
      font-weight: 400;
      line-height: 18px;
      letter-spacing: 0.15px;
    }
  }

  .btn_more {
    background: linear-gradient(33deg, #ffcc21 8.75%, #ff963c 86.64%);
    color: #fff;
    text-align: center;
    font-family: Hiragino Kaku Gothic Pro;
    font-size: 18px;
    font-style: normal;
    font-weight: 300;
    line-height: 26px;
    width: 296px;
    padding: 15px 0;
    display: flex;
    align-items: center;
    justify-content: center;
    margin: 0 auto;
    border: none;
    border-radius: 4px;
    cursor: pointer;
    transition: 0.5s;

    &:hover {
      transition: 0.5s;
      opacity: 0.5;
      border: none;
      color: #fff;
      transition: 0.5s;
    }
  }
`;

export default FoodLibrary;

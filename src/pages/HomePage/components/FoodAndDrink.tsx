import React from 'react';

// * Styles
import styled from 'styled-components';

// * Icon
import IconKnife from 'assets/images/icon_knife.svg';
import IconCup from 'assets/images/icon-cup.svg';

const FoodAndDrink = () => {
  return (
    <FoodAndDrinkWrapper>
      <div className="item">
        <img src={IconKnife} alt="" />
        <span>Morning</span>
      </div>
      <div className="item">
        <img src={IconKnife} alt="" />
        <span>Lunch</span>
      </div>
      <div className="item">
        <img src={IconKnife} alt="" />
        <span>Dinner</span>
      </div>
      <div className="item">
        <img src={IconCup} alt="" />
        <span>Snack</span>
      </div>
    </FoodAndDrinkWrapper>
  );
};

const FoodAndDrinkWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-bottom: 24px;
  max-width: 736px;
  margin: 0 auto 24px auto;

  .item {
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    background: url('../images/component_hex.png');
    background-size: contain;
    background-position: center;
    background-repeat: no-repeat;
    width: 134px;
    height: 116px;

    img {
      max-width: 40px;
    }

    span {
      color: #fff;
      text-align: center;
      font-family: 'Inter', sans-serif;
      font-size: 20px;
      font-style: normal;
      font-weight: 400;
      line-height: 24px;
    }
  }
`;

export default FoodAndDrink;

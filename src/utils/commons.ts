import { format } from 'date-fns';

export const DATETIME_FORMAT = 'yyyy.MM.dd. a hh:mm:ss';

export const STORAGE_KEYS = {
  LAST_SELECTED_LANGUAGE: 'LAST_SELECTED_LANGUAGE'
};

export const convertTime = (time: string | number) => {
  const sec_num = parseInt(time.toString(), 10);
  let hours: string | number = Math.floor(sec_num / 3600);
  let minutes: string | number = Math.floor((sec_num - hours * 3600) / 60);
  let seconds: string | number = sec_num - hours * 3600 - minutes * 60;

  if (hours < 10) {
    hours = '0' + hours;
  }
  if (minutes < 10) {
    minutes = '0' + minutes;
  }
  if (seconds < 10) {
    seconds = '0' + seconds;
  }
  return minutes + ':' + seconds;
};

export const formatDatetime = (datetime: number, formatString = DATETIME_FORMAT) => {
  return format(datetime, formatString);
};

export const getLocalStorage = (key: string) => {
  return window?.localStorage.getItem(key);
};

export const setLocalStorage = (key: string, value: string) => {
  return window?.localStorage.setItem(key, value);
};

import { lazy } from 'react';
import { Redirect } from 'react-router-dom';
import { IRoutes } from './Routes.d';
import { RoutesString } from './RoutesString';

const ErrorLayout = lazy(() => import('layouts/ErrorLayout'));
const HomeLayout = lazy(() => import('layouts/HomeLayout'));
const HomePage = lazy(() => import('pages/HomePage'));
const ColumnPage = lazy(() => import('pages/ColumnPage'));
const MyRecordPage = lazy(() => import('pages/MyRecordPage'));
const LoginPage = lazy(() => import('pages/LoginPage'));
const AuthGuard = lazy(() => import('guards/AuthGuard'));
const AnonymousGuard = lazy(() => import('guards/AnonymousGuard'));

export const RoutesConfig: IRoutes[] = [
  {
    layout: HomeLayout,
    path: RoutesString.Auth,
    guard: AnonymousGuard,
    routes: [
      {
        path: RoutesString.Login,
        page: LoginPage,
        exact: true,
      },
      {
        path: '*',
        page: () => <Redirect to={RoutesString.Login} />,
      },
    ],
  },
  {
    layout: HomeLayout,
    path: RoutesString.Home,
    routes: [
      {
        path: RoutesString.Home,
        page: HomePage,
        guard: AuthGuard,
        exact: true,
      },
      {
        path: RoutesString.MyRecord,
        page: MyRecordPage,
        guard: AuthGuard,
        exact: true,
      },
      {
        path: RoutesString.Column,
        page: ColumnPage,
        exact: true,
      },
      {
        path: '*',
        page: () => <Redirect to={RoutesString.Login} />,
      },
    ],
  },
  {
    path: '*',
    page: () => <Redirect to={RoutesString.Login} />,
  },
];

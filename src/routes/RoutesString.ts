export const RoutesString = {
  Home: '/home',
  Column: '/home/column',
  MyRecord: '/home/my-record',
  Error: '/error',
  NotFound: '/error/not-found',
  AccessDenied: '/error/access-denied',
  Auth: '/auth',
  Login: '/auth/login',
};

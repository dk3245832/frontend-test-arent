export const ENDPOINT = {
    SUBTITLES: '/subtitles',
    LIST_COMMUNITIES: '/api/v1/users/me/communities',
    LIST_VIDEOS: '/api/v1/video/subtitle/list',
    LIST_LANGUAGES: '/api/v1/video/languages',
};

import React from "react";
import styled from "styled-components";

// * Images

const scrollToTop = () => {
  const c = document.documentElement.scrollTop || document.body.scrollTop;
  if (c > 0) {
    window.requestAnimationFrame(scrollToTop);
    window.scrollTo(0, c - c / 10);
  }
};

export const ScrollBack = ({ scrollMonClass = "scrollDefault" }) => (
  <ScrollTop className={`scrollBackWrapper ${scrollMonClass}`}>
    <div onClick={() => scrollToTop()}>
      <svg
        xmlns="http://www.w3.org/2000/svg"
        width="48"
        height="48"
        viewBox="0 0 48 48"
        fill="none"
      >
        <path
          d="M24 0.5C36.9787 0.5 47.5 11.0213 47.5 24C47.5 36.9787 36.9787 47.5 24 47.5C11.0213 47.5 0.5 36.9787 0.5 24C0.5 11.0213 11.0213 0.5 24 0.5Z"
          fill="white"
          fill-opacity="0.01"
          stroke="#777777"
        />
        <path
          d="M30.5852 28.042L24.0002 21.6579L17.4151 28.042L16.5389 27.1925L24.0002 19.959L31.4614 27.1925L30.5852 28.042Z"
          fill="#777777"
        />
      </svg>
    </div>
  </ScrollTop>
);

const ScrollTop = styled.div`
  position: fixed;
  top: 50%;
  right: 0;
  cursor: pointer;

  &.scrollAtTop,
  &.scrollDefault {
    opacity: 0;
    transition: opacity 400ms cubic-bezier(0.4, 0, 0.2, 1) 0ms,
      transform 250ms cubic-bezier(0.4, 0, 0.2, 1) 0ms;
  }

  &.scrollInitiated {
    opacity: 1;
    transition: opacity 400ms cubic-bezier(0.4, 0, 0.2, 1) 0ms,
      transform 250ms cubic-bezier(0.4, 0, 0.2, 1) 0ms;
  }
`;

const ButtonGoTop = styled.div`
  .arrow-up {
    width: 0;
    height: 0;
    border-left: 5px solid transparent;
    border-right: 5px solid transparent;

    border-bottom: 5px solid black;
  }
`;

import { useState, useEffect } from "react";

const ScrollMonitor = () => {
  const [scrollPos, setScrollPos] = useState("scrollDefault");

  const handleScroll = () => {
    if (window.scrollY <= 20) {
      setScrollPos("scrollAtTop");
    } else if (window.scrollY > 300) {
      setScrollPos("scrollInitiated");
    }
  };

  useEffect(() => {
    window.addEventListener("scroll", () => handleScroll());
  });

  return scrollPos;
};

export default ScrollMonitor;

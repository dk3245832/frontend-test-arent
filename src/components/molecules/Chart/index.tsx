import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend,
} from "chart.js";
import { Line } from "react-chartjs-2";

// * Styles
import styled from "styled-components";

import * as faker from "faker";

ChartJS.register(
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend
);

export const options = {
  responsive: true,
  plugins: {
    legend: {
      position: "top" as const,
      display: false,
    },
    title: {
      display: true,
    },
  },
  scales: {
    x: {
      grid: {
        color: "gray",
      },
      ticks: {
        color: "white",
      },
      border: {
        display: false,
      },
    },
    y: {
      ticks: {
        display: false,
        beginAtZero: true,
      },
      grid: {
        drawBorder: false,
        display: false,
      },
      border: {
        display: false,
      },
    },
  },
};

const labels = [
  "1月",
  "2月",
  "3月",
  "4月",
  "5月",
  "6月",
  "7月",
  "8月",
  "9月",
  "10月",
  "11月",
  "12月",
];

const data = {
  labels,
  datasets: [
    {
      label: "Dataset 1",
      data: labels.map(() => faker.datatype.number({ min: -1000, max: 1000 })),
      borderColor: "#FFCC21",
      backgroundColor: "#FFCC21",
    },
    {
      label: "Dataset 2",
      data: labels.map(() => faker.datatype.number({ min: -1000, max: 1000 })),
      borderColor: "#8FE9D0",
      backgroundColor: "#8FE9D0",
    },
  ],
};

const Chart = () => {
  return (
    <MyExerciseWrapper>
      <Line id="myChart" options={options} data={data} />
    </MyExerciseWrapper>
  );
};

const MyExerciseWrapper = styled.div`
  background: #414141;
  padding: 16px 56px 24px 24px;
  height: 100%;
  width: 100%;

  canvas {
    height: 100% !important;
    width: 100% !important;
  }

  .head-table {
    display: flex;
    column-gap: 23px;
    margin-bottom: 4px;

    h2 {
      color: #fff;
      font-family: Inter;
      font-size: 15px;
      font-style: normal;
      font-weight: 400;
      line-height: 18px;
      letter-spacing: 0.15px;

      span {
        display: block;
      }
    }

    p {
      color: #fff;
      font-family: Inter;
      font-size: 22px;
      font-style: normal;
      font-weight: 400;
      line-height: 27px;
      letter-spacing: 0.11px;
    }
  }
`;

export default Chart;

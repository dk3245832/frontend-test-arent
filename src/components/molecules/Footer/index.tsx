import React from "react";

// * Styles
import styled from "styled-components";

// * Icon Images
import Logo from "assets/images/logo.png";
import IconMemo from "assets/images/icon_memo.svg";
import IconInfo from "assets/images/icon_info.svg";
import IconChallenge from "assets/images/icon_challenge.svg";

const Header = () => {
  return (
    <FooterWrapper>
      <HeaderContent>
        <ul className="menu">
          <li>
            <a href="">会員登録</a>
          </li>
          <li>
            <a href="">運営会社</a>
          </li>
          <li>
            <a href="">利用規約</a>
          </li>
          <li>
            <a href="">個人情報の取扱について</a>
          </li>
          <li>
            <a href="">特定商取引法に基づく表記</a>
          </li>
          <li>
            <a href="">お問い合わせ</a>
          </li>
        </ul>
      </HeaderContent>
    </FooterWrapper>
  );
};

const FooterWrapper = styled.div`
  background: #414141;
  width: 100%;
`;

const HeaderContent = styled.div`
  display: flex;
  flex-wrap: nowrap;
  justify-content: space-between;
  align-items: center;
  width: 990px;
  padding: 56px 0;
  margin: 0 auto;

  .logo {
    img {
      max-width: 109px;
    }
  }

  ul {
    list-style: none;

    li {
      display: inline-flex;
      align-items: center;
      margin: 0 20px 0;

      img {
        max-width: 32px;
      }

      p {
        position: relative;
        margin-right: 7px;

        span {
          position: absolute;
          top: -2px;
          right: -5px;
          background: #ea6c00;
          color: #ffffff;
          width: 16px;
          height: 16px;
          border-radius: 50%;
          font-size: 10px;
          display: flex;
          justify-content: center;
          align-items: center;
        }
      }

      a {
        color: #ffffff;
        text-decoration: none;
        font-family: Hiragino Kaku Gothic Pro;
        font-size: 11px;
        font-style: normal;
        font-weight: 300;
        line-height: 16px; /* 145.455% */
        letter-spacing: 0.033px;
        transition: 0.5s;

        &:hover {
          color: #ff963c;
          transition: 0.5s;
        }
      }
    }
  }
`;

export default Header;

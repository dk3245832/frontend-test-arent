import React from 'react';

// * Styles
import styled from 'styled-components';

// * Icon Images
import Logo from 'assets/images/logo.png';
import IconMemo from 'assets/images/icon_memo.svg';
import IconInfo from 'assets/images/icon_info.svg';
import IconChallenge from 'assets/images/icon_challenge.svg';
import { Link } from 'react-router-dom';
import { RoutesString } from 'routes/RoutesString';

// * Components
import Burger from "./Components/Burger";
import Menu from "./Components/Menu";

const Header = () => {
  const [open, setOpen] = React.useState(false);
  return (
    <HeaderWrapper>
      <HeaderContent>
        <div className="logo">
          <Link to={RoutesString.Home}>
            <img src={Logo} alt="" />
          </Link>
        </div>
        <ul className="menu">
          <li>
            <img src={IconMemo} alt="" />
            <Link to={RoutesString.MyRecord} href="">
              自分の記録
            </Link>
          </li>
          <li>
            <img src={IconChallenge} alt="" />
            <Link to={RoutesString.Column} href="">
              チャレンジ
            </Link>
          </li>
          <li>
            <p>
              <img src={IconInfo} alt="" />
              <span>1</span>
            </p>
            <Link to="" href="">
              お知らせ
            </Link>
          </li>
          <Burger open={open} setOpen={setOpen} />
          <Menu open={open} setOpen={setOpen} />
        </ul>
      </HeaderContent>
    </HeaderWrapper>
  );
};

const HeaderWrapper = styled.div`
  background: #414141;
  box-shadow: 0px 3px 6px 0px rgba(0, 0, 0, 0.16);
  top: 0;
  left: 0;
  width: 100%;
  z-index: 9;
`;

const HeaderContent = styled.div`
  display: flex;
  flex-wrap: nowrap;
  justify-content: space-between;
  align-items: center;
  width: 990px;
  padding: 0 12px;
  margin: 0 auto;
  position: relative;

  .logo {
    img {
      max-width: 109px;
    }
  }

  ul {
    list-style: none;
    display: inline-flex;
    align-items: center;
    margin-bottom: 0;

    li {
      display: inline-flex;
      align-items: center;
      margin: 0 20px 0;

      img {
        max-width: 32px;
      }

      p {
        position: relative;
        margin-right: 7px;
        margin-bottom: 0;

        span {
          position: absolute;
          top: -2px;
          right: -5px;
          background: #EA6C00;
          color: #ffffff;
          width: 16px;
          height: 16px;
          border-radius: 50%;
          font-size: 10px;
          display: flex;
          justify-content: center;
          align-items: center;
        }
      }

      a {
        color: #ffffff;
        text-decoration: none;
        font-size: 16px
        line-height: 23px;
        transition: 0.5s;

        
        &:hover {
          color: #FF963C;
          transition: 0.5s;
        }
      }
    }
  }
`;

export default Header;

import React from 'react';
import { Link } from 'react-router-dom';
import { RoutesString } from 'routes/RoutesString';
import styled from 'styled-components';

const StyledMenu = styled.nav`
  display: flex;
  flex-direction: column;
  justify-content: center;
  background: #777777;
  width: 248px;
  text-align: left;
  position: absolute;
  top: 52px;
  right: 12px;
  transition: transform 0.3s ease-in-out;

  &.open {
    display: flex;
  }

  &.closed {
    display: none;
  }

  ul {
    list-style: none;
    display: flex;
    margin-bottom: 0;
    flex-direction: column;
    align-items: flex-start;

    li {
      padding: 0;
      margin: 0;
      border-bottom: 1px solid #fff;
      width: 100%;
      border-bottom: 2px solid #686868b0;
    }

    a {
      color: #fff;
      font-family: 'HiraginoKakuGothicPro';
      font-size: 18px;
      font-style: normal;
      font-weight: 300;
      line-height: 26px;
      padding: 23px 0 23px 32px;
      display: inline-block;
      width: 100%;
      border-top: 1px solid #ffffff26;

      &:hover {
        color: #343078;
      }
    }
  }
`;

interface MenuProps {
  open: boolean;
  setOpen: React.Dispatch<React.SetStateAction<boolean>>;
}

const Menu: React.FC<MenuProps> = ({ open, setOpen }) => {
  return (
    <StyledMenu className={open ? 'open' : 'closed'}>
      <ul>
        <li>
          <Link to={RoutesString.MyRecord}>自分の記録</Link>
        </li>
        <li>
          <a href="/">体重グラフ</a>
        </li>
        <li>
          <a href="/">目標</a>
        </li>
        <li>
          <a href="/">選択中のコース</a>
        </li>
        <li>
          <Link to={RoutesString.Column}>コラム一覧</Link>
        </li>
        <li>
          <a href="/">設定</a>
        </li>
      </ul>
    </StyledMenu>
  );
};

export default Menu;

interface SliderFill {
  circleRadius: number;
  startPoint: {
    x: number;
    y: number;
  };
  endPoint: {
    x: number;
    y: number;
  };
  largeArcFlag: number;
  sweepFlag: number;
  onClickHandler: (e: any) => void;
}

const SliderFill: React.FC<SliderFill> = ({
  circleRadius,
  startPoint,
  endPoint,
  largeArcFlag,
  sweepFlag,
  onClickHandler,
  ...props
}) => {
  return (
    <>
      <defs>
        <linearGradient id="slider-fill-gradient" y1="100%" y2="0%">
          <stop offset="0%" stopColor="#8189ff"></stop>
          <stop offset="100%" stopColor="#3f3cff"></stop>
        </linearGradient>
      </defs>
      <path
        strokeWidth="1"
        strokeLinecap="round"
        fill="none"
        stroke="#fff"
        d={`
          M ${startPoint.x}, ${startPoint.y}
          A ${circleRadius} ${circleRadius} 0 ${largeArcFlag} ${sweepFlag} ${endPoint.x},${endPoint.y}
        `}
        style={{
          transform: 'rotate(0deg)',
          transformOrigin: 'center center',
        }}
        onClick={(e) => onClickHandler(e.nativeEvent)}
      />
    </>
  );
};

export default SliderFill;

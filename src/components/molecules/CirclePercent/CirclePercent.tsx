import SliderTrack from './SliderTrack';
import SliderThumb from './SliderThumb';
import SliderFill from './SliderFill';

import styled from 'styled-components';
import useSliderActions from './hooks/useSliderActions';

interface ISliderProps {
  clockwise: boolean;
}

const Slider: React.FC<ISliderProps> = ({ clockwise, ...props }) => {
  const {
    isDragging,
    onCoordinatesChange,
    percentage,
    x,
    y,
    originX,
    originY,
    angleDegrees,
    radius,
    sliderWrapper,
    setIsDragging,
  } = useSliderActions();

  return (
    <>
      <div style={{ position: 'relative' }}>
        <svg
          ref={sliderWrapper}
          height="400px"
          width="400px"
          viewBox="0 0 100 100"
          pointerEvents="visiblePainted"
        >
          <SliderTrack
            position={{ x: originX, y: originY }}
            radius={radius}
            onClickHandler={onCoordinatesChange}
          />

          <SliderFill
            circleRadius={radius}
            startPoint={{ x: originX, y: originY - radius }}
            endPoint={{ x, y }}
            largeArcFlag={angleDegrees > 180 ? 1 : 0}
            sweepFlag={clockwise ? 1 : 0}
            onClickHandler={onCoordinatesChange}
          />

          <SliderThumb
            position={{ x, y }}
            isDragging={isDragging}
            onClick={onCoordinatesChange}
            startDragging={() => setIsDragging(true)}
            stopDragging={() => setIsDragging(false)}
          />
        </svg>
        <div
          style={{
            position: 'absolute',
            top: 0,
            display: 'flex',
            width: '100%',
            height: '100%',
            justifyContent: 'center',
            alignItems: 'center',
            pointerEvents: 'none',
          }}
        >
          <Weapper>
            <Text>05/21</Text>
            <Percent>{percentage}%</Percent>
          </Weapper>
        </div>
      </div>
    </>
  );
};

export default Slider;

const Weapper = styled.div`
  display: flex;
  align-items: center;
  gap: 5px;
`;

const Text = styled.div`
  color: #fff;
  text-align: center;
  text-shadow: 0px 0px 6px #fc7400;
  font-family: Inter;
  font-size: 18px;
  font-style: normal;
  font-weight: 400;
  line-height: 22px;
  margin-bottom: -3px;
`;

const Percent = styled.div`
  color: #fff;
  text-align: center;
  text-shadow: 0px 0px 6px #fca500;
  font-family: Inter;
  font-size: 25px;
  font-style: normal;
  font-weight: 400;
  line-height: 30px;
`;

interface ISliderTrackProps {
  position: {
    x: number;
    y: number;
  };
  radius: number;
  onClickHandler: (e: any) => void;
}

const SliderTrack: React.FC<ISliderTrackProps> = ({
  position,
  radius,
  onClickHandler,
  ...props
}) => {
  return (
    <circle
      cx={position.x}
      cy={position.y}
      r={radius}
      strokeWidth="1"
      stroke=""
      fill="none"
      onClick={(e) => onClickHandler(e.nativeEvent)}
    />
  );
};

export default SliderTrack;

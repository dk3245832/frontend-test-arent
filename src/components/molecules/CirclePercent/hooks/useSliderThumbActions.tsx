import { useEffect, useCallback } from 'react';

interface IUseSliderThumbActions {
  onClick: (e: any) => void;
  isDragging: boolean;
  stopDragging: () => void;
}

const useSliderThumbActions = ({
  isDragging,
  onClick,
  stopDragging,
}: IUseSliderThumbActions) => {
  const onMouseMove = useCallback(
    (e: any) => {
      if (isDragging) {
        onClick(e);
      }
    },
    [onClick, isDragging]
  );

  const onMouseUp = useCallback(() => {
    if (isDragging) {
      stopDragging();
    }
  }, [isDragging, stopDragging]);

  useEffect(() => {
    document.addEventListener('mousemove', onMouseMove);
    document.addEventListener('mouseup', onMouseUp);

    /* Avoid setting up multiple events and memory leak */
    return () => {
      document.removeEventListener('mousemove', onMouseMove);
      document.removeEventListener('mouseup', onMouseUp);
    };
  }, [onMouseMove, onMouseUp]);

  return { onMouseMove };
};

export default useSliderThumbActions;

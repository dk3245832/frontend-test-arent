import React from 'react';

import { useState, useEffect, useRef } from 'react';

const useSliderActions = () => {
  const [originX] = useState(50);
  const [originY] = useState(50);
  const [radius] = useState(30);
  const [angleDegrees, setAngleDegrees] = useState(223);
  const [angleRadians, setAngleRadians] = useState(0);
  const [percentage, setPercentage] = useState(0);
  const [x, setX] = useState(0);
  const [y, setY] = useState(0);

  const sliderWrapper = useRef<SVGSVGElement | null>(null);

  const [isDragging, setIsDragging] = useState(false);

  const getSvgInfo = () => {
    const { width, height, x, y } =
      sliderWrapper.current?.getBoundingClientRect() as DOMRect;
    const cx = width / 1 + x;
    const cy = height / 1 + y;
    return { cx, cy };
  };

  const onCoordinatesChange = (e: any) => {
    const { clientX: mouseX, clientY: mouseY } = e;
    const { cx, cy } = getSvgInfo();

    const radians = Math.atan2(mouseX - cx, cy - mouseY);
    let degrees = radians * (180 / Math.PI);
    if (degrees < 0) degrees = 360 + degrees;

    setAngleDegrees(degrees);
  };

  useEffect(() => {
    setAngleRadians(angleDegrees * (Math.PI / 180));

    setX(originX + radius * Math.sin(angleRadians));
    setY(originY + radius * -Math.cos(angleRadians));

    setPercentage(Math.round((angleDegrees / 360) * 100));
  }, [angleDegrees, angleRadians, originX, originY, radius]);

  return {
    percentage,
    x,
    y,
    isDragging,
    onCoordinatesChange,
    originX,
    originY,
    sliderWrapper,
    radius,
    angleDegrees,
    setIsDragging,
  };
};

export default useSliderActions;

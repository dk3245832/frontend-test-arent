import { useSliderThumbActions } from './hooks';

interface ISliderThumbProps {
  position: {
    x: number;
    y: number;
  };
  onClick: (e: any) => void;
  startDragging: (e: any) => void;
  isDragging: boolean;
  stopDragging: () => void;
}

const SliderThumb: React.FC<ISliderThumbProps> = ({
  position,
  onClick,
  startDragging,
  isDragging,
  stopDragging,
  ...props
}) => {
  const { onMouseMove } = useSliderThumbActions({
    isDragging,
    onClick,
    stopDragging,
  });

  return (
    <>
      <circle cx={position.x} cy={position.y} pointerEvents="none" />
      <circle
        cx={position.x}
        cy={position.y}
        onTouchStart={startDragging}
        onClick={(e) => onClick(e.nativeEvent)}
        onMouseDown={startDragging}
        onMouseMove={(e) => onMouseMove(e.nativeEvent)}
      />
    </>
  );
};

export default SliderThumb;

import create from 'zustand';
import { persist } from 'zustand/middleware';

interface ITokens {
  accessToken: string;
  refreshToken: string;
}

interface IAuthState {
  user: object;
  isLoading: false;
  rebound: true;
  loginAction: () => void;
  logoutAction: () => void;
  isLoggedIn: boolean;
}
const useAuthStore = create<IAuthState>()(
  persist(
    (set) => ({
      user: {},
      isLoading: false,
      rebound: true,
      isLoggedIn: false,
      loginAction: () => {
        set({
          isLoggedIn: true,
        });
      },
      logoutAction: () => {
        set({
          user: {},
          isLoading: false,
          rebound: true,
        });
      },
    }),
    {
      name: 'auth',
    }
  )
);

export default useAuthStore;

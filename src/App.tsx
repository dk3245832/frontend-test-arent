import Routes from "./routes";
import ScrollMonitor from "./components/molecules/ScrollMonitor";
import { ScrollBack } from "./components/molecules/ScrollBackUp";

function App() {
  const scrollPosition = ScrollMonitor();

  return (
    <>
      <ScrollBack scrollMonClass={scrollPosition} />
      <Routes />
    </>
  );
}

export default App;

import React from 'react';
import homeLayoutStyles from './HomeLayout.module.scss';

// * Styles
import styled from 'styled-components';

// * components
import { Header, Footer } from 'components/molecules';

interface IHomeLayout {
  children?: React.ReactNode;
}
const HomeLayout: React.FC<IHomeLayout> = ({ children }) => {
  return (
    <HomeLayOutWrapper>
      <Header />
      <div className={homeLayoutStyles.wrapper}>{children}</div>
      <Footer />
    </HomeLayOutWrapper>
  );
};

const HomeLayOutWrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  height: 100vh;
`;

export default HomeLayout;

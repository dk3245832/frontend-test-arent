import React from 'react';
import { Redirect } from 'react-router-dom';
import { RoutesString } from 'routes/RoutesString';
import useAuthStore from 'stores/auth.store';

const AnonymousGuard: React.FC<any> = ({ children }) => {
  const { isLoggedIn } = useAuthStore();

  // TODO: Update this one if there is an access token
  if (isLoggedIn) {
    return <Redirect to={RoutesString.Home} />;
  }

  return children;
};

export default AnonymousGuard;
